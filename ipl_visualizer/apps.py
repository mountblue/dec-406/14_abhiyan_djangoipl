from django.apps import AppConfig


class IplVisualizerConfig(AppConfig):
    name = 'ipl_visualizer'
