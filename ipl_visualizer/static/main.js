/**
 * Name:main.js
 * Description : JavaScript file for IPL App.To convert data to required form and plot highchart
 * Input: Question number representing the ipl 
 * Output: HighChart Visualization
 * author: abhiyan timilsina
 */

//Declaring Global Functions
const BASEURL = '/ipl/ques_';
const plotInfo = {
    '1' : {
        'subtitle' : 'Number of Matches Played in All years of IPL',
        'x-axis' : 'Years',
        'yAxis' : {
            'title' : 'Number of Matches Played'
        },
        'keyfield' : 'season',
        'valuefield' : 'no_of_matches'
    },
    '2' : {
        'subtitle' : 'Number of Matches won by all team in all years of IPL',
        'x-axis' : 'Team Name',
        'yAxis' : {
            'title' : 'Number of Matches Won'
        }    
    },
    '3' : {
        'subtitle' : 'Extra Runs Conceded by Each Team 2016',
        'yAxis' : {
            'title' : 'Extra Runs'
        },
        'x-axis' : 'Team Name',
        'keyfield' : 'bowling_team',
        'valuefield' : 'runs_conceded'
   
    },
    '4' : {
        'subtitle' : 'Economy Bowlers IPL 2015',
        'x-axis' : 'Player Name',
        'yAxis' : {
            'title' : 'Bowler Economy'
        },
        'keyfield' : 'bowler',
        'valuefield' : 'economy'
            
    },
    '5' : {
        'subtitle' : 'Number of Matches Played in each Stadium',
        'x-axis' : 'Stadium Name',
        'yAxis' : {
            'title' : 'Number of Matches Played'
        },
        'keyfield' : 'venue',
        'valuefield' : 'matches_played'
    
    }
}


//Graph plot for problem 1,3,4,5
const PlotGraph = (plotData,id)=>{
    Highcharts.chart('container', {
        chart: {
            type: 'column'
        },
        title: {
            text: 'IPL Visualization 2008-2018'
        },
        subtitle: {
            text: plotInfo[id]['subtitle'] 
        },
        xAxis: {
            categories : plotInfo[id]['categories'] 
        },
        yAxis: plotInfo[id]['yAxis'],
        series : [{
            name : plotInfo[id]['x-axis'],
            data : plotInfo[id]['series'] 
        }] 
       
    }); 
}

//Load The chart data from json endpoints
const loadChart  = (id)=>{
    
    let completeURL = BASEURL + id;
    $.get( completeURL, ( plotData ) => {
        console.log(plotData)
        if(id!=2) {
            plotInfo[id]['categories'] = plotData.map((entries)=>{
                
                return entries[plotInfo[id]['keyfield']]
            });
            plotInfo[id]['series'] = plotData.map((entries)=>{
                console.log()
                return entries[plotInfo[id]['valuefield']]
            });
        
        PlotGraph(plotData,id);     

    }
    else {
        years = new Array(10).fill(0).map((value,index)=>2008+index);
        plotInfo[id]['categories'] = new Array(10).fill(0).map((value,index)=>2008+index)
        team_yearly_wincount = {}
        plotData.forEach((team_wins)=>{
            if(team_yearly_wincount[team_wins['winner']]==undefined) {
                team_yearly_wincount[team_wins['winner']] = new Array(10).fill(0);
            }
            team_yearly_wincount[team_wins['winner']][years.indexOf(team_wins['season'])] = team_wins['matches_won'] 
        })
       plotInfo[id]['series'] =  Object.entries(team_yearly_wincount).map((team_info)=>{
            return {name:team_info[0] , data:team_info[1]}
       });
       Highcharts.chart('container', {
        chart: {
            type: 'column'
        },
        title: {
            text: 'IPL Visualization 2008-2018'
        },
        subtitle: {
            text: plotInfo[id]['subtitle'] 
        },
        xAxis: {
            categories : plotInfo[id]['categories'] 
        },
        plotOptions: {
            column: {
                stacking: 'normal',
                dataLabels: {
                    enabled: true,
                    color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white'
                }
            }
        },
        yAxis: plotInfo[id]['yAxis'],
        series : plotInfo[id]['series']
       
    });
    }
});
}


