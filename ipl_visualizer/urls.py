''' Name : ipl_visualizer/urls.py
    description : To map the urls for the ipl app.It contains 5 JSON view and one HTML view with slug.
    author : abhiyantimilsina
'''


from django.urls import path,include
from . import views
urlpatterns = [
    path('ques_1/', views.matches_all_years, name='ques1'), 
    path('ques_2/', views.matches_won_teams_all_years, name='ques2'), 
    path('ques_3/', views.extra_runs_conceded_2016, name='ques3'), 
    path('ques_4/', views.most_economy_player_2015, name='ques4'), 
    path('ques_5/', views.stadium_matches_count, name='ques5'),
    path('<int:question_id>/', views.index, name='highcharts'),
]
