'''
Name : ipl_visualizer/views.py
description : This is the main controller of the application responsible for quering the model and rendering templates.
author : abhiyantimilsina
'''

from django.shortcuts import render
from .models import Matches, Deliveries
from django.db.models import Sum, Count, FloatField
from django.core.cache.backends.base import DEFAULT_TIMEOUT
from django.views.decorators.cache import cache_page
from django.conf import settings
from django.http import JsonResponse, HttpResponse

#Get the CACHETTL from settings
CACHE_TTL = getattr(settings, 'CACHE_TTL', DEFAULT_TIMEOUT)


@cache_page(CACHE_TTL)
def matches_all_years(request):
    query_result_set = Matches.objects.values('season').annotate(no_of_matches = Count('season')).order_by('season')
    # Return JSON response with safe as False(required)
    return JsonResponse([entry for entry in list(query_result_set)], safe=False)


@cache_page(CACHE_TTL)
def matches_won_teams_all_years(request):
    query_result_set = Matches.objects.values('season', 'winner').annotate(matches_won =Count('winner')).exclude(winner__exact='').order_by('winner')
    return JsonResponse([entry for entry in list(query_result_set)], safe=False)


@cache_page(CACHE_TTL)
def most_economy_player_2015(request):
    query_result_set = Deliveries.objects.values('bowler').filter(match_id__in=Matches.objects.filter(season__exact=2015)).annotate(economy=Sum('total_runs', output_field=FloatField()) / Count('over', distinct=True, output_field=FloatField())).order_by('economy')[:5]
    return JsonResponse([entry for entry in list(query_result_set)], safe=False)


@cache_page(CACHE_TTL)
def extra_runs_conceded_2016(request):
    query_result_set = Deliveries.objects.values('bowling_team').filter(match_id__in=Matches.objects.filter(season__exact=2016)).annotate(runs_conceded=Sum('extra_runs')).order_by('runs_conceded')
    return JsonResponse([entry for entry in list(query_result_set)], safe=False)


@cache_page(CACHE_TTL)
def stadium_matches_count(request):
    query_result_set = Matches.objects.values('venue').annotate(matches_played=Count('venue')).order_by('matches_played')
    return JsonResponse([entry for entry in list(query_result_set)], safe=False)

# Render HTML template which contains JS code for drawing HighChart
@cache_page(CACHE_TTL)
def index(request, question_id):
    return render(request, 'index.html', context={'id': question_id})
